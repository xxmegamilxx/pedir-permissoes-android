package net.megamil.pedindopermissoes;

import android.app.Activity;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_CONTACTS;

/**
 * Criado por Eduardo dos santos em 25/11/2017.
 * Megamil.net
 */

public class MainActivity extends AppCompatActivity {

    // TODO: Para proxíma atualização:
    //Usar "onRequestPermissionsResult" para atualizar os TextView na hora correta.

    TextView permissaoMapa;
    TextView permissaoCamera;
    TextView permissaoAlbum;
    TextView permissaoContatos;
    TextView permissaoEscrita;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Encapsulando os campos
        permissaoMapa     = findViewById(R.id.permissaoMapa);
        permissaoCamera   = findViewById(R.id.permissaoCamera);
        permissaoAlbum    = findViewById(R.id.permissaoAlbum);
        permissaoContatos = findViewById(R.id.permissaoContatos);
        permissaoEscrita  = findViewById(R.id.permissaoEscrita);

        //Instanciando objeto para solicitar as permissões, também armazenando a referência a activity
        final Activity activity = this;
        final pedindoPermissoes pp = new pedindoPermissoes();

        //Colocando o click na label
        permissaoMapa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(pp.solicitarPermissao(activity,ACCESS_FINE_LOCATION)){//Permissão concedida.
                    permissaoMapa.setText(R.string.comPermissaoMapa);
                    permissaoMapa.setTextColor(Color.GREEN);
                } else { //Permissão Recusada.
                    permissaoMapa.setText(R.string.permissaoMapa);
                    permissaoMapa.setTextColor(Color.RED);
                }
            }
        });

        //Usar camera
        permissaoCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(pp.solicitarPermissao(activity,CAMERA)){//Permissão concedida.
                    permissaoCamera.setText(R.string.comPermissaoCamera);
                    permissaoCamera.setTextColor(Color.GREEN);
                } else { //Permissão Recusada.
                    permissaoCamera.setText(R.string.permissaoCamera);
                    permissaoCamera.setTextColor(Color.RED);
                }
            }
        });

        //Ler Fotos e vídeos.
        permissaoAlbum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(pp.solicitarPermissao(activity,READ_EXTERNAL_STORAGE)){//Permissão concedida.
                    permissaoAlbum.setText(R.string.comPermissaoAlbum);
                    permissaoAlbum.setTextColor(Color.GREEN);
                } else { //Permissão Recusada.
                    permissaoAlbum.setText(R.string.permissaoAlbum);
                    permissaoAlbum.setTextColor(Color.RED);
                }
            }
        });

        //Ler os contatos
        permissaoContatos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(pp.solicitarPermissao(activity,READ_CONTACTS)){//Permissão concedida.
                    permissaoContatos.setText(R.string.comPermissaoContatos);
                    permissaoContatos.setTextColor(Color.GREEN);
                } else { //Permissão Recusada.
                    permissaoContatos.setText(R.string.permissaoContatos);
                    permissaoContatos.setTextColor(Color.RED);
                }
            }
        });

        //Gravar no aparelho.
        permissaoEscrita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(pp.solicitarPermissao(activity,WRITE_EXTERNAL_STORAGE)){//Permissão concedida.
                    permissaoEscrita.setText(R.string.comPermissaoEscrita);
                    permissaoEscrita.setTextColor(Color.GREEN);
                } else { //Permissão Recusada.
                    permissaoEscrita.setText(R.string.permissaoEscrita);
                    permissaoEscrita.setTextColor(Color.RED);
                }
            }
        });

    }
}
