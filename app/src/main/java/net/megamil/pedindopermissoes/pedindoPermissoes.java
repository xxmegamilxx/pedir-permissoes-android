package net.megamil.pedindopermissoes;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

/**
 * Criado por Eduardo dos santos em 25/11/2017.
 * Megamil.net
 */

public class pedindoPermissoes {

    public boolean solicitarPermissao(Activity contexto, String Tipo){
        // Se não possui permissão
        if (ContextCompat.checkSelfPermission(contexto,Tipo) != PackageManager.PERMISSION_GRANTED) {
            // Verifica se já mostramos o alerta e o usuário negou na 1ª vez.
            if (ActivityCompat.shouldShowRequestPermissionRationale(contexto, Tipo)) {
                // Caso o usuário tenha negado a permissão anteriormente, e não tenha marcado o check "nunca mais mostre este alerta"
                // Podemos mostrar um alerta explicando para o usuário porque a permissão é importante.
                //ActivityCompat.requestPermissions(contexto,new String[]{WRITE_EXTERNAL_STORAGE},0);
                ActivityCompat.requestPermissions(contexto,new String[]{Tipo},0);
                System.out.println("SOLICITANDO PERMISSÃO PARA ("+Tipo+"), NÃO É A PRIMEIRA VEZ");
                return false;
            } else {
                // Solicita a permissão
                ActivityCompat.requestPermissions(contexto,new String[]{Tipo},0);
                System.out.println("SOLICITANDO PERMISSÃO PARA ("+Tipo+"), PRIMEIRA VEZ");
                return true;
            }
        } else {
            System.out.println("ACESSO CONCEDIDO");
            return true;
        }

    }

}
