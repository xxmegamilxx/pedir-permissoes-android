Pedir Permissões Android:

Objetivo:
	O objetivo desse projeto é ser usado como base para projetos onde seja necessária a solicitação de permissões do sistema android.

Como funciona:
	É criada uma classe genérica que pode realizar o pedido de qualquer tipo de permissão,
	apresentando a caixa de dialogo e exibindo etapa por etapa no LogCat.